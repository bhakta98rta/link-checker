package com.project.linkchecker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.io.StringWriter;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import java.util.concurrent.TimeUnit;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


public class LinkChecker {
	
	static WebDriver driver;
	final static VelocityEngine ve = new VelocityEngine();
	static Template t ;
	static String screenshotPath;
    static ArrayList list = new ArrayList();
    static Map map = new HashMap();
    static String filepath;


	public static void main(String[] args) throws Exception {
		
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/resource/drivers/chromedriver");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        ve.init();
        t = ve.getTemplate( "templates/linkchecker.vm");
		
		String dateTime=new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		File f=new  File(System.getProperty("user.dir")+"/results/"+dateTime);
		f.mkdirs();
		filepath =f.getAbsolutePath();

		screenshotPath =filepath+"/screenshots";
		f=new File(screenshotPath);
		f.mkdirs();
		
		 CSVReader reader = new CSVReader(new FileReader(System.getProperty("user.dir")+"/resource/data/www-lrn-com_20150205T101334Z_CrawlErrors.csv"));
		 String [] nextLine;
		 int count = 0;
	     while ((nextLine = reader.readNext()) != null) {
	        // nextLine[] is an array of values from the line
	        System.out.println(nextLine[0]);
	        if(!nextLine[0].toString().equals("URL")){
	        	URL aURL = new URL(nextLine[0].toString());
	        	String host = aURL.getHost();
	        	System.out.println(host);
	        	int statusCode;
	        	if (host.equals("www.lrn.com")){
	        		statusCode = navigateToUrlAndTakeScreenshots(nextLine[0].toString(),count);
	        	}else{
	        		statusCode = getStatusCode(nextLine[0].toString());
	        	}
	        	writeResultToCsv(nextLine[0].toString(),statusCode,getStatusMsg(statusCode));
		        count++;
	        }

//	        if (count > 0){
//	        	break;
//	        }
	     }
	     
        VelocityContext context = new VelocityContext();
        context.put("resultList", list);

        
        //bgcolor="#FF0000"
        
		StringWriter writer = new StringWriter();
		t.merge( context, writer );
		File file = new File(filepath+"/report.html");
		file.createNewFile();
		FileWriter content = new FileWriter(file); 
		content.write(writer.toString()); 
		content.flush();
		content.close();
		driver.close();
		reader.close();
	}
	
	public static int getStatusCode(String url) throws ClientProtocolException, IOException{
		//If anything goes wrong in my code then return 606
		int responseCode = 606;
		CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpGet httpGet = new HttpGet(url);
            CloseableHttpResponse response1 = httpclient.execute(httpGet);

            try {
                responseCode = response1.getStatusLine().getStatusCode();

//                HttpEntity entity1 = response1.getEntity();
//
//                InputStreamReader reader = new InputStreamReader(entity1.getContent());
//        		BufferedReader r1 = new BufferedReader(reader);
//        		String str = null;
//        		while((str = r1.readLine()) != null)
//        		{
//        			System.out.println(str);
//        		}
//        		
//                EntityUtils.consume(entity1);
            } 

            finally {
                response1.close();
            }
        }
        catch(HttpHostConnectException e){
        	//System.out.println(e.toString());
        	responseCode = 599;
        }
        finally {
            httpclient.close();
        }
		return responseCode;
	}
	
	public static int navigateToUrlAndTakeScreenshots(String url, int filename) throws InterruptedException, IOException{
    	int status = 0;
		driver.get(url);
    	Thread.sleep(8000);

    	File scrFile = (File) ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		
    	FileUtils.copyFile(scrFile, new File(screenshotPath+"/"+filename+".png"));
     	map = new HashMap();
    	String currentUrl = driver.getCurrentUrl();
    	if(currentUrl.equals(url.toString())){
    		if(driver.getPageSource().contains("page not found"))
    		{
    			status = 404;
    		}else{
    			status = 200;
    		}
    		map.put("style", "bgcolor=''");
    		map.put("font", "style=''");
    	}else{
    		if(driver.getPageSource().contains("page not found"))
    		{
    			status = 404;
    		}else{
    			status = 302;
    		}
    		map.put("style", "bgcolor='#FF0000'");
    		map.put("font", "style='color: white;'");
    	}
    	map.put("actualURL", currentUrl.toString());
     	map.put("inputURL", url.toString());
        map.put("screenShots", screenshotPath+"/"+filename+".png");
        list.add( map );
        return status;
	}
	
	public static void writeResultToCsv(String url, int status, String extraInfo) throws IOException{

		FileWriter mFileWriter = new FileWriter(filepath+"/report.csv", true);
		CSVWriter writer = new CSVWriter(mFileWriter, ',');

	     // feed in your array (or convert your data to an array)
		String statusCode = String.valueOf(status);
	     String[] entries = {url,statusCode,extraInfo};
	     writer.writeNext(entries);
		 writer.close();
	}
	
	public static String getStatusMsg(int statusCode){
		String statusMessage = "";
		
		switch (statusCode){
		case 200:
			statusMessage = "Successfully Loaded";
			break;
		case 302:
			statusMessage = "Url redirected to home page";
			break;
		case 404:
			statusMessage = "Page Not Found";
			break;
		case 500:
			statusMessage = "Internal Server Error";
			break;
		case 599:
			statusMessage = "Timed Out, Server may be down";
			break;
		case 503:
			statusMessage = "Service Unavailable";
			break;
		case 502:
			statusMessage = "Bad Gateway";
			break;
		default:
			statusMessage = "";
		}
		return statusMessage;
	}
}
